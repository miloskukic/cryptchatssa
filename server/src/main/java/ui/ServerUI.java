package ui;

import server.Server;

import java.awt.event.ActionEvent;
import javax.swing.*;
import java.awt.*;

public class ServerUI extends ServerFrame implements Server.ServerResponse{

    private Server server;

    public ServerUI(Server server) {
        super();
        initListeners();
        this.server = server;
        server.setServerResponse(this);
    }


    private void initListeners() {
        btn_connect.addActionListener(this::connect);
        btn_encrypt.addActionListener(this::encrypt);
        btn_decrypt.addActionListener(this::decrypt);
        btn_send.addActionListener(this::send);
        btnCancel.addActionListener(this::clear);
        btnKey.addActionListener(this::newKey);
    }

    private void connect(ActionEvent e) {
        try {
            server.setPort(Integer.parseInt(txt_port.getText()));
            new Thread(server).start();
        } catch (Exception ex) {
            error(null, ex.getMessage());
            txt_port.setText("");
        }
    }

    private void encrypt(ActionEvent e) {
        String message = txt_message.getText();
        server.encryptMessage(message);
        notification("src/main/resources/lock_dialog.png","Poruka je sifrovana!");
    }

    private void newKey(ActionEvent e) {
        server.generateNewKey();
        notification("src/main/resources/key_dialog.png", "Server razmenjuje kljuceve...");
    }

    private void send(ActionEvent e) {
        String message = txt_messageEncrypted.getText();
        server.sendMessage(message);
        notification("src/main/resources/lock_dialog.png", "Poruka je poslata");

    }

    private void decrypt(ActionEvent e) {
        String encrypted = txt_messageReceived.getText();
        server.decryptMessage(encrypted);
        notification("src/main/resources/unlock_dialog.png", "Poruka je desifrovana!");
    }

    private void clear(ActionEvent e) {
        txt_message.setText("");
    }

    @Override
    public void showEncryptedMessage(String s) {
        txt_messageEncrypted.setText(s);
    }

    @Override
    public void showDecryptedMessage(String s) {
        txt_messageReceived.setText(s);
    }

    @Override
    public void notifyConnectionOpen() {
        getTxt_status().setText("Ceka se klijent...");
        getTxt_status().setForeground(Color.WHITE);
        getTxt_status().setBackground(Color.decode("#003366"));
    }

    @Override
    public void notifyMessageReceived() {
        notification("src/main/resources/android_logo.png", "Upravo ste primili poruku" );
    }

    @Override
    public void showMessageReceived(String s) {
        txt_messageReceived.setText(s);
    }

    @Override
    public void showPrivateKey(String s) {
        txt_key.setText(s);
    }

    @Override
    public void showErrorMessage(String s) {
        error("Greska", s);
    }

    @Override
    public void notifyConnectionEstablished() {
        txt_status.setText("server.Server je povezan");
        txt_status.setForeground(Color.BLACK);
        txt_status.setBackground(Color.green);
        notification("src/main/resources/android_logo.png", "Server je sada povezan sa klijentom");
    }
}
