import { Component, OnInit } from '@angular/core';
import { Hero } from '../models/hero';
import { HEROES } from '../mock-heroes';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  selectedHero: Hero;

  // selectedHero: Hero ={
  //   id:1,
  //   name: "Windstorm"
  // }

  hero: Hero ={
    id:1,
    name: "Windstorm"
  }

  //hero = "Windstorm";

  heroes: Hero[];

  constructor() { }

  ngOnInit(): void {
    this.heroes = HEROES;
  }

  onSelect(hero:Hero){
    this.selectedHero = hero;
  }

}
