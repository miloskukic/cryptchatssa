import {Student} from "./models/student";

export const STUDENT: Student[] = [
    { index: 1217, name: 'Studnet 1', surname:'Test1',avrg:29.8 },
    { index: 8417, name: 'Studnet 2', surname:'Test2',avrg:30.8 },
    { index: 53517, name: 'Studnet 3', surname:'Test3',avrg:21.8 },
    { index: 2417, name: 'Studnet 4', surname:'Test4',avrg:22.8 },
]