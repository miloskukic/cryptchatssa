import { Component, OnInit } from '@angular/core';
import { STUDENT } from '../mock-students';
import { Student } from '../models/student';


@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.students = STUDENT;
  }

  students:Student[];

}
